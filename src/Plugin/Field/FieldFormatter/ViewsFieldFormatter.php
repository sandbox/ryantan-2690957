<?php

/**
 * @file
 * Contains
 *   \Drupal\formatter_views\Plugin\Field\FieldFormatter\ViewsFieldFormatter.
 */

namespace Drupal\formatter_views\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * @FieldFormatter(
 *  id = "formatter_views",
 *  label = @Translation("Embedded View"),
 *   description = @Translation("Display embedded view of the referenced view."),
 *  field_types = {
 *   "entity_reference",
 *   }
 * )
 */
class ViewsFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'arguments' => [],
    ];
  }

  /**
   * Returns array of default argument info.
   *
   * @return array
   */
  protected function getDefaultArguments() {
    return [
      'entity_id' => $this->t('Entity ID'),
      'custom' => $this->t('Custom'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // This formatter is only available for entity reference fields targetting views.
    return $field_definition->getFieldStorageDefinition()
      ->getSetting('target_type') == 'view';
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $element['arguments'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('View Arguments'),
        $this->t('Value'),
        $this->t('Weight'),
      ],
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'arguments-order-weight',
        ],
      ],
      '#caption' => $this->t('Select and reorder which arguments (contextual filters) to send to the view when rendering.'),
    ];

    $default_arguments = $this->getSetting('arguments');

    foreach ($this->getDefaultArguments() as $argument_id => $argument_name) {
      $default_arguments[$argument_id]['label'] = $argument_name;
    }
    foreach ($default_arguments as $argument_id => $argument_settings) {
      $row = [
        'checked' => [
          '#type' => 'checkbox',
          '#title' => $argument_settings['label'],
          '#default_value' => $argument_settings['checked'],
        ],
        'custom_value' => [
          '#markup' => '',
        ],
        'weight' => [
          '#type' => 'weight',
          '#title' => $this->t('Weight for @title', ['@title' => $argument_settings['label']]),
          '#title_display' => 'invisible',
          '#attributes' => ['class' => ['arguments-order-weight']],
        ],
        '#attributes' => ['class' => ['draggable']],
      ];
      if ($argument_id === 'custom') {
        $row['custom_value'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Custom value'),
          '#title_display' => 'invisible',
          '#attributes' => ['class' => ['arguments-custom-value']],
          '#default_value' => $argument_settings['custom_value'],
        ];
      }

      // Add row to table.
      $element['arguments'][$argument_id] = $row;
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();
    $all_arguments = $this->getDefaultArguments();

    // Get list of selected arguments.
    $arguments = array_filter($settings['arguments'], function ($argument) {
      return $argument['checked'];
    });

    $argument_labels = [];
    foreach ($arguments as $argument_id => $argument_settings) {
      $argument_labels[] = $all_arguments[$argument_id] . ($argument_id === 'custom' ? ' (' . $argument_settings['custom_value'] . ')' : '');
    }

    // Add an empty state.
    if (empty($argument_labels)) {
      $argument_labels[] = $this->t('None');
    }

    return [t('Argument(s): @arguments', ['@arguments' => implode(', ', $argument_labels)])];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    // @TODO: Support multi-value fields?

    /** @var FieldItemInterface $item */
    $item = $items[0];

    if ($item) {
      // Get view name from field value.
      $view_name = isset($item->getValue()['target_id']) ? $item->getValue()['target_id'] : NULL;

      // Abort if no view name can be found.
      if ($view_name) {

        // Load user-selected arguments.
        $settings = $this->getSettings();
        $user_arguments = array_filter($settings['arguments'], function ($argument) {
          return $argument['checked'];
        });

        $arguments = [];
        // Prepare actual argument values.
        foreach ($user_arguments as $argument_id => $argument_settings) {
          switch ($argument_id) {
            case 'entity_id':
              $arguments[$argument_id] = $items->getParent()->getValue()->id();
              break;

            case 'custom':
              $arguments[$argument_id] = $argument_settings['custom_value'];
              break;
          }
        }

        // @TODO: Let user define view display.
        $display_name = 'default';

        $elements[] = [
          '#type' => 'view',
          '#name' => $view_name,
          '#display_id' => $display_name,
          '#arguments' => array_values($arguments),
        ];
      }
    }

    return $elements;
  }

}
